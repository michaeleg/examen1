﻿using appcell3.vistas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appcell3
{
    public partial class frmPrincipal : Form
    {
        private DataSet dspersona;
        private BindingSource bsperona;
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void gestionDeCelularToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGestionPersona gp = new frmGestionPersona();
            gp.MdiParent = this;
            gp.Dspersona = dspersona;
            gp.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {

        }
    }
}
