﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appcell3.vistas
{
    public partial class frmGestionPersona : Form
    {
        private DataSet dspersona;
        private BindingSource bspersona;

        public DataSet Dspersona
        {
            get
            {
                return dspersona;
            }
            set
            {
                dspersona = value;
            }
        }

        public frmGestionPersona()
        {
            InitializeComponent();
            bspersona = new BindingSource();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmPersona c = new frmPersona();
            c.ShowDialog();
        }

        private void frmGestionCelular_Load(object sender, EventArgs e)
        {
            bspersona.DataSource = Dspersona;
            bspersona.DataMember = Dspersona.Tables["Persona"].TableName;
            dgvPersona.DataSource = bspersona;
            dgvPersona.AutoGenerateColumns = true;
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            try {
                bspersona.Filter = string.Format("nombre like '*{0}*'", txtBuscar.Text);

             }catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
