﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace appcell3.vistas
{
    public partial class frmPersona : Form
    {
        public frmPersona()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNombre.Text = " ";
            txtApellido.Text = " ";
            txtCedula.Text = "";
            txtDireccion.Text = " ";
            txtTelefono.Text = " ";
            txtGmail.Text = " ";
            

        }
    }
}
