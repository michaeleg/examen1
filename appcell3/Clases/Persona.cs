﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appcell3.Clases
{
    class Persona
    {
        private string nombre;
        private string apellido;
        private int cedula;
        private string direccion;
        private string gmail;
        private string telefono;

        public Persona(string nombre, string apellido, int cedula, string direccion, string gmail, string telefono)
        {
            this.nombre = nombre;
            this.apellido = apellido;
            this.cedula = cedula;
            this.direccion = direccion;
            this.gmail = gmail;
            this.telefono = telefono;
        }

        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public int Cedula { get => cedula; set => cedula = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Gmail { get => gmail; set => gmail = value; }
        public string Telefono { get => telefono; set => telefono = value; }
    }
}
